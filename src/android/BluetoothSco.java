package gntikos.plugin.bluetoothsco;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.util.Log;


public class BluetoothSco extends CordovaPlugin {
    
    private final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
    
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        
        if(this.cordova.getActivity().isFinishing()) return true;
        
        if (action.equals("start")) {
            String audioMode = "";
            
            try {
                audioMode = args.getString(0);
            } catch (JSONException e) {
                // Just catch the damn exception!
            }
            
            this.startScoConnection(callbackContext, audioMode);
            return true;
        } 
        else if (action.equals("stop")) {
            this.stopScoConnection(callbackContext);
            return true;
        }
        
        return false;
    }
    

    private synchronized void startScoConnection(final CallbackContext callbackContext, final String targetAudioMode) {
        final CordovaInterface cordova = this.cordova;
        
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                if (btAdapter == null) {
                    callbackContext.error("This device does not support Bluetooth");
                    return;
                } else if (! btAdapter.isEnabled()) {
                    callbackContext.error("Bluetooth headset is disconnected. Would you like to monitor manually?");
                    return;
                } 
                
                cordova.getActivity().registerReceiver(new BroadcastReceiver() {

                    @Override
                    public void onReceive(Context context, Intent intent) {
                        int state = intent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_STATE, -1);
                        Log.d("CordovaLog", "state: " + state);
                        if (state == AudioManager.SCO_AUDIO_STATE_CONNECTED) {
                            callbackContext.success();
                            context.unregisterReceiver(this);
                        } /*if(state == AudioManager.SCO_AUDIO_STATE_DISCONNECTED) {                       	
                        	context.unregisterReceiver(this);
                        	callbackContext.error("Bluetooth headset is disconnected. Would you like to monitor manually?");
                        }*/
                    }
                }, new IntentFilter(AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED));
                
                AudioManager audioManager = (AudioManager) cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
                
                if (! audioManager.isBluetoothScoAvailableOffCall()) {
                    callbackContext.error("Off-call Bluetooth audio not supported on this device.");
                    return;
                }
                
                if (targetAudioMode.equals("in_call")) {
                    audioManager.setMode(AudioManager.MODE_IN_CALL);
                } else if (targetAudioMode.equals("in_communication")) {
                    audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
                } else if (targetAudioMode.equals("normal")) {
                    audioManager.setMode(AudioManager.MODE_NORMAL);
                }
                
                audioManager.setBluetoothScoOn(true);
                
                try{
                audioManager.startBluetoothSco();
            }
                catch(NullPointerException ex){
                	callbackContext.error("Bluetooth headset is disconnected. Would you like to monitor manually?");
                }
            }
        };
        
        this.cordova.getActivity().runOnUiThread(runnable);
    }
    
    private synchronized void stopScoConnection(final CallbackContext callbackContext) {
        
        final CordovaInterface cordova = this.cordova;
        
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                AudioManager audioManager = (AudioManager) cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
                
                try {
                    audioManager.setBluetoothScoOn(false);
                    audioManager.stopBluetoothSco();
                    audioManager.setMode(AudioManager.MODE_NORMAL);
                    callbackContext.success();
                } catch (Exception e) {
                    callbackContext.error(e.getMessage());
                }
            }
        };
        
        this.cordova.getActivity().runOnUiThread(runnable);
    }

}
